using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GroundPrefabPair
{
    public GameObject prefab1;
    public GameObject prefab2;
}

public class MapGenerate : MonoBehaviour
{
    public GroundPrefabPair[] groundPrefabPairs; // Assign your ground prefab pairs to this array in the Inspector.
    public Transform playerTransform;

    private float spawnZ = 0f; // Z-coordinate where the next ground prefab pair will spawn.
    private float groundLength; // Length of the ground prefab.

    private int currentPrefabPairIndex = 0; // Index of the current ground prefab pair in the array.

    private void Start()
    {
        groundLength = groundPrefabPairs[currentPrefabPairIndex].prefab1.GetComponent<Renderer>().bounds.size.z;
    }

    private void Update()
    {
        // Check if the player has moved past the current spawn point.
        if (playerTransform.position.z > spawnZ - (groundLength / 2))
        {
            SpawnGroundPair();
        }
    }

    private void SpawnGroundPair()
    {
        // Calculate the position for the next ground prefab pair.
        Vector3 spawnPosition = new Vector3(0f, 0f, spawnZ + groundLength);

        // Spawn the current ground prefab pair at the calculated position.
        Instantiate(groundPrefabPairs[currentPrefabPairIndex].prefab1, spawnPosition, Quaternion.identity);
        spawnPosition += Vector3.forward * groundLength;

        Instantiate(groundPrefabPairs[currentPrefabPairIndex].prefab2, spawnPosition, Quaternion.identity);

        // Update the spawn position for the next prefab pair.
        spawnZ += groundLength * 2; // Double the length to account for two prefabs.

        // Move to the next prefab pair in the array (loop back to the first if at the end).
        currentPrefabPairIndex = (currentPrefabPairIndex + 1) % groundPrefabPairs.Length;
        groundLength = groundPrefabPairs[currentPrefabPairIndex].prefab1.GetComponent<Renderer>().bounds.size.z;
    }
}
