using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AI.Waypoint
{
    public class PedestrianSpawner : MonoBehaviour
    {
        public GameObject[] PedetrianPrefab;
        public int NumPedetrian;

        private void Start()
        {
            StartCoroutine(Spawn());
        }

        IEnumerator Spawn()
        {
            for (int i = 0; i < NumPedetrian; i++)
            {
                GameObject obj = Instantiate(PedetrianPrefab[Random.Range(0, PedetrianPrefab.Length)]);
                Transform child = transform.GetChild(Random.Range(0, transform.childCount - 1));
                obj.GetComponent<WaypointNavigator>().currentWP = child.GetComponent<Waypoint>();
                obj.transform.position = child.position;
                
                yield return new WaitForEndOfFrame();
            }
        }
    }
}