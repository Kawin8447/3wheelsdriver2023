using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public string sceneToLoad; // The name of the scene to load

    public GameObject UiToDisplay;

    public void SceneToLoad()
    {
        SceneManager.LoadScene(sceneToLoad);
    }
    
    public void ToggleUI()
    {
        UiToDisplay.SetActive(!UiToDisplay.activeSelf);
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
}
