using System;
using System.Collections;
using System.Collections.Generic;
//using Palmmedia.ReportGenerator.Core;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class SimpleGenerate : MonoBehaviour
{
    public GameObject[] section;
    public GameObject goalSection;
    public int ZPos = 50;
    public int MaxZpos = 900;
    private int secNum;
    //public float totalTime = 60f; // Total time for generation in seconds
    private float currentTime = 0f;
    private bool generating = false;
    private bool goal = false;
    private bool goalBreak = true;

    private void Start()
    {
        if (!generating)
        {
            generating = true;
            StartCoroutine(GenerateSections());
        }

        goal = false;
    }

    private void Update()
    {
        // Check if the total generation time has elapsed
        if (ZPos < MaxZpos)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            // Stop generating sections
            generating = false;
            goal = true;
        }
        

        if (goal == true && goalBreak == true)
        {
            GenerateGoal();
            goalBreak = false;
        }
    }

    IEnumerator GenerateSections()
    {
        while (generating)
        {
            secNum = Random.Range(0, section.Length);
            Instantiate(section[secNum], new Vector3(0, 0, ZPos), Quaternion.identity);
            ZPos += 50;
            yield return new WaitForSeconds(2);
        }
    }

    private void GenerateGoal()
    {
        Instantiate(goalSection, new Vector3(0, 0, ZPos), Quaternion.identity);
    }
}
