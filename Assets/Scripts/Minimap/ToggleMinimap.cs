using System;
using UnityEngine;

namespace Minimap
{
    public class ToggleMinimap : MonoBehaviour
    {
        [SerializeField] private GameObject _minimapGO;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                OpenMinimap();
            }
        }

        public void OpenMinimap()
        {
            _minimapGO.SetActive(true);
            Time.timeScale = 0f;
        }
        
        public void CloseMinimap()
        {
            _minimapGO.SetActive(false);
            Time.timeScale = 1.0f;
        }
    }
}