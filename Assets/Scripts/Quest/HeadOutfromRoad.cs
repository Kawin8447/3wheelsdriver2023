using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Quest
{
    public class HeadOutfromRoad : MonoBehaviour
    {
        public GameObject npcUI;
        private Transform UIpos;
        private int _footpathSequence;

        private void Awake()
        {
             UIpos= npcUI.GetComponent<Transform>();
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.collider.CompareTag("South Footpath"))
                _footpathSequence = 1;
            else if (other.collider.CompareTag("North Footpath")) 
                _footpathSequence = 2;
            else if (other.collider.CompareTag("East Footpath"))
                _footpathSequence = 3;
            else if (other.collider.CompareTag("West Footpath"))
                _footpathSequence = 4;


            switch (_footpathSequence)
            {
                case 1:
                    //Debug.Log("S footpath touched");
                    transform.rotation = Quaternion.Euler(0f,90f,0f);
                    UIpos.rotation = Quaternion.Euler(-90f, 0f, 90f);
                    break;
                case 2:
                    //Debug.Log("N footpath touched");
                    transform.rotation = Quaternion.Euler(0f,-90f,0f);
                    UIpos.rotation = Quaternion.Euler(-90f, 0f, 90f);
                    break;
                case 3:
                    //Debug.Log("E footpath touched");
                    transform.rotation = Quaternion.Euler(0f,0f,0f);
                    UIpos.rotation = Quaternion.Euler(-90f, 0f, 90f);
                    break;
                case 4:
                    //Debug.Log("W footpath touched");
                    transform.rotation = Quaternion.Euler(0f,-180f,0f);
                    UIpos.rotation = Quaternion.Euler(-90f, 0f, 90f);
                    break;
                default:
                    //Debug.Log("nothing...");
                    transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                    UIpos.rotation = Quaternion.Euler(0f, 0f, 0f);
                    break;
            }
        }
    }
}