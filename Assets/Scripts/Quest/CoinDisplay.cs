using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoinDisplay : MonoBehaviour
{
    public TextMeshProUGUI coinDisplayText;
    
    // Update is called once per frame
    void Update()
    {
        coinDisplayText.text = ("Coins: " + GlobalManager.Instance.myCoins.ToString("0") + "/3000");
    }
}
