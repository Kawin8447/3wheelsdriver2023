using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Setup : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GlobalManager.Instance.accelerator = 1;
        GlobalManager.Instance.coins = 0;
        
        GlobalManager.Instance.playerSpeed = 0f;
        GlobalManager.Instance.turnSpeed = 0f;
        
        StartCoroutine(DelayedActionCoroutine());
    }

    private IEnumerator DelayedActionCoroutine()
    {
        yield return new WaitForSeconds(1.5f);

        GlobalManager.Instance.playerSpeed = 15f;
        GlobalManager.Instance.turnSpeed = 7f;
    }
}
