using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInteraction : MonoBehaviour
{
    private GameObject heldObject;
    private bool isHolding = false;

    public GameObject pickupCollider; // The GameObject with a collider used to check for objects

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && !isHolding)
        {
            // Check if the player clicked the left mouse button and is not currently holding an object
            Collider[] hitColliders = Physics.OverlapSphere(pickupCollider.transform.position, 0.1f); // Adjust the radius as needed

            foreach (Collider col in hitColliders)
            {
                if (col.CompareTag("Grabbable"))
                {
                    // Check if the object is tagged as "Grabbable"
                    heldObject = col.gameObject;
                    isHolding = true;
                    // Attach the object to the player's position
                    heldObject.transform.parent = transform;
                    break; // Stop searching for objects to pick up
                }
            }
        }
        else if (Input.GetMouseButtonUp(0) && isHolding)
        {
            // Check if the player released the left mouse button and is holding an object
            isHolding = false;
            // Detach the object from the player and let it fall
            heldObject.transform.parent = null;
        }
    }
}
