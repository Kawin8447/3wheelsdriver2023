using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Panel : MonoBehaviour
{
    private System.Random random = new System.Random();

    public TextMeshProUGUI panelText;

    public int randomNumber;
    public int symbolIndex;

    public string symbol;

    private float finalResult;

    public Collider triggerPanelCollider;

    private void Start()
    {
        // Generate a random number between 1 and 5 (inclusive)
        this.randomNumber = random.Next(1, 6);

        // Generate a random index to select a mathematical symbol
        this.symbolIndex = random.Next(0, 4);

        // Define an array of possible mathematical symbols
        string[] symbols = { "+", "-", "*", "/" };

        // Get the selected symbol
        this.symbol = symbols[symbolIndex];

        // Display the random number and symbol
        Debug.Log("Random Number: " + this.symbol + this.randomNumber);
        //Debug.Log("Random Symbol: " + this.symbol);
        
        panelText.text = "" + this.symbol + this.randomNumber;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && GlobalManager.Instance.panelTrigger == true)
        {
            // Perform a simple calculation based on the random number and symbol
            float result = Calculate(randomNumber, symbol);
            Debug.Log("Result: " + result);

            GlobalManager.Instance.accelerator = result;
            //result = finalResult;
            //GlobalManager.Instance.accelerator = finalResult;
            
            GlobalManager.Instance.panelTrigger = false;
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (GlobalManager.Instance.panelTrigger == false)
        {
            StartCoroutine(DelayedActionCoroutine());
        }
    }

    private IEnumerator DelayedActionCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        GlobalManager.Instance.panelTrigger = true;
    }
    
    public float Calculate(int number, string symbol)
    {
        float result = 0;

        switch (symbol)
        {
            case "+":
                result = GlobalManager.Instance.accelerator + number;
                Debug.Log("do cal");
                break;
            case "-":
                result = GlobalManager.Instance.accelerator - number;
                Debug.Log("do cal");
                break;
            case "*":
                result = GlobalManager.Instance.accelerator * number;
                Debug.Log("do cal");
                break;
            case "/":
                if (number != 0)
                {
                    result = (float)GlobalManager.Instance.accelerator / number;
                }
                Debug.Log("do cal");
                break;
            default:
                Debug.LogError("Invalid symbol: " + symbol);
                break;
        }

        return result;
    }

    /*public void Start()
    {
        if (GlobalManager.Instance.accelerator < 500)
        {
            // Generate a random number between 1 and 5 (inclusive)
            int randomNumber = random.Next(1, 6);

            // Generate a random index to select a mathematical symbol
            int symbolIndex = random.Next(0, 2);

            // Define an array of possible mathematical symbols
            string[] symbols = { "+", "*" };

            // Get the selected symbol
            string symbol = symbols[symbolIndex];

            // Display the random number and symbol
            panelText.text = "" + symbol + randomNumber;
            //symbolText.text = "Random Symbol: " + symbol;

            // Perform a simple calculation based on the random number and symbol
            float result = Calculate(randomNumber, symbol);
            Debug.Log("Result: " + result);
        }
        if (GlobalManager.Instance.accelerator >= 500 && GlobalManager.Instance.accelerator < 1000)
        {
            // Generate a random number between 1 and 5 (inclusive)
            int randomNumber = random.Next(1, 6);

            // Generate a random index to select a mathematical symbol
            int symbolIndex = random.Next(0, 4);

            // Define an array of possible mathematical symbols
            string[] symbols = { "+", "-", "*", "/" };

            // Get the selected symbol
            string symbol = symbols[symbolIndex];

            // Display the random number and symbol
            panelText.text = "" + symbol + randomNumber;
            //symbolText.text = "Random Symbol: " + symbol;

            // Perform a simple calculation based on the random number and symbol
            float result = Calculate(randomNumber, symbol);
            Debug.Log("Result: " + result);
        }
        if (GlobalManager.Instance.accelerator > 1000)
        {
            // Generate a random number between 1 and 5 (inclusive)
            int randomNumber = random.Next(1, 6);

            // Generate a random index to select a mathematical symbol
            int symbolIndex = random.Next(0, 2);

            // Define an array of possible mathematical symbols
            string[] symbols = { "-", "/" };

            // Get the selected symbol
            string symbol = symbols[symbolIndex];

            // Display the random number and symbol
            panelText.text = "" + symbol + randomNumber;
            //symbolText.text = "Random Symbol: " + symbol;

            // Perform a simple calculation based on the random number and symbol
            float result = Calculate(randomNumber, symbol);
            Debug.Log("Result: " + result);
        }
    }*/
}
