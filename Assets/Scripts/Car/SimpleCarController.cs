using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCarController : MonoBehaviour
{
    public float moveSpeed = 10f; // Speed of the car's forward and backward movement
    public float turnSpeed = 100f; // Speed of the car's turning
    public float wheelieForce = 500f; // Force applied to lift the car's front end

    private float horizontalInput, verticalInput;
    private Rigidbody carRigidbody;

    private void Start()
    {
        carRigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        // Get input for movement and turning
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        // Check for the "S" key
        if (Input.GetKey(KeyCode.S))
        {
            // Apply a force to lift the car's front end
            carRigidbody.AddForce(Vector3.up * wheelieForce);
        }
    }

    private void FixedUpdate()
    {
        // Move the car forward and backward
        Vector3 moveDirection = transform.forward * verticalInput;
        carRigidbody.velocity = moveDirection * moveSpeed;

        // Rotate the car for turning
        Vector3 turnDirection = Vector3.up * horizontalInput;
        Quaternion turnRotation = Quaternion.Euler(turnDirection * turnSpeed * Time.fixedDeltaTime);
        carRigidbody.MoveRotation(carRigidbody.rotation * turnRotation);
    }
}
