using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPanel : MonoBehaviour
{
    public static SoundPanel Instance;
    
    public GameObject UiToDisplay;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleUI();
        }
    }

    public void ToggleUI()
    {
        UiToDisplay.SetActive(!UiToDisplay.activeSelf);
    }
}
