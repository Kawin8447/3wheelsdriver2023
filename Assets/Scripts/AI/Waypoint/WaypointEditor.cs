using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace AI.Waypoint
{
    [InitializeOnLoad()]
    public class WaypointEditor 
    {
        [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Pickable)]
        public static void OnDrawSceneGizmo(Waypoint wp, GizmoType gType)
        {
            if ((gType & GizmoType.Selected) != 0)
            {
                Gizmos.color = Color.yellow;
            }
            else
            {
                Gizmos.color = Color.yellow * 0.5f;
            }
            
            Gizmos.DrawSphere(wp.transform.position, .1f);

            Gizmos.color = Color.white;
            Gizmos.DrawLine(wp.transform.position + (wp.transform.right * wp.width / 2f), 
                wp.transform.position - (wp.transform.right * wp.width / 2f));

            if (wp.previousWP != null)
            {
                Gizmos.color = Color.red;
                Vector3 offset = wp.transform.right * wp.width / 2f;
                Vector3 offsetTo = wp.previousWP.transform.right * wp.previousWP.width / 2f;
                
                Gizmos.DrawLine(wp.transform.position + offset, wp.previousWP.transform.position + offsetTo);
            }
            if (wp.nextWP != null)
            {
                Gizmos.color = Color.green;
                Vector3 offset = wp.transform.right * -wp.width / 2f;
                Vector3 offsetTo = wp.nextWP.transform.right * -wp.nextWP.width / 2f;
                
                Gizmos.DrawLine(wp.transform.position + offset, wp.nextWP.transform.position + offsetTo);
            }

            if (wp.branches != null)
            {
                foreach (Waypoint branch in wp.branches)
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawLine(wp.transform.position, branch.transform.position);
                }
            }
        }
    }
}