using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed; // Forward movement speed
    public float horizontalSpeed; // Horizontal movement speed

    public float rushPower;
    public float slowdownRate = 15f;
    private Rigidbody rb;
    private float currentSpeed;

    private bool rushBreak = false;

    private void Update()
    {
        moveSpeed = GlobalManager.Instance.playerSpeed;
        horizontalSpeed = GlobalManager.Instance.turnSpeed;
        
        // Forward Movement
        Vector3 forwardMovement = transform.forward * moveSpeed * Time.deltaTime;
        transform.position += forwardMovement;

        // Horizontal Movement (left and right)
        float horizontalInput = Input.GetAxis("Horizontal");
        Vector3 horizontalMovement = Vector3.right * horizontalInput * horizontalSpeed * Time.deltaTime;
        transform.position += horizontalMovement;

        //rushPower = GlobalManager.Instance.accelerator;
        
        if (GlobalManager.Instance.rushing == true)
        {
            StartCoroutine(WaitAndCallNextFunction());
        }

        if (rushBreak == true)
        {
            slowDown();
        }

        if (GlobalManager.Instance.accelerator >= 1000)
        {
            rushPower = 20;
        }
        else if(GlobalManager.Instance.accelerator > 500 && GlobalManager.Instance.accelerator < 1000)
        {
            rushPower = 15;
        }
        else if (GlobalManager.Instance.accelerator <= 500)
        {
            rushPower = 10;
        }

        Debug.Log("" + rushPower);

        if (GlobalManager.Instance.accelerator <= 0)
        {
            GlobalManager.Instance.accelerator = 1;
        }
    }

    private void rusher()
    {
        rb = GetComponent<Rigidbody>();
        currentSpeed = rushPower;
        rb.velocity = transform.forward * rushPower;
    }
    
    // Coroutine that waits for 3 seconds
    private IEnumerator WaitAndCallNextFunction()
    {
        yield return new WaitForSeconds(2.5f); // Wait for 3 seconds

        // Call the next function or perform any action you want here
        
        rusher();

        GlobalManager.Instance.rushing = false;

        rushBreak = true;
    }

    private void slowDown()
    {
        // Gradually slow down the object's speed
        currentSpeed -= slowdownRate * Time.deltaTime;

        // Clamp the speed to prevent it from going negative
        currentSpeed = Mathf.Max(currentSpeed, 0f);

        // Apply the updated velocity to the Rigidbody
        rb.velocity = transform.forward * currentSpeed;

        // Check if the object has come to a stop
        if (currentSpeed <= 0f)
        {
            // Object has stopped, you can perform additional actions here
            Debug.Log("done");

            StartCoroutine(DelayedActionCoroutine());
        }

        if (currentSpeed != 0f)
        {
            GlobalManager.Instance.calSumFinalCoins = GlobalManager.Instance.calSumFinalCoins + (currentSpeed/10000f);
        }
    }
    
    private IEnumerator DelayedActionCoroutine()
    {
        yield return new WaitForSeconds(3f);

        Debug.Log("Delayed action performed after 3 seconds.");

        GlobalManager.Instance.myCoins = GlobalManager.Instance.myCoins + (GlobalManager.Instance.coins * GlobalManager.Instance.calSumFinalCoins);
        GlobalManager.Instance.coins = 0;
        GlobalManager.Instance.accelerator = 0;
        GlobalManager.Instance.calSumFinalCoins = 1;
        SoundManager.Instance.musicSource.Stop();
        SceneManager.LoadScene("Scenes/Main_Level");
    }
}
