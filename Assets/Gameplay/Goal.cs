using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public GameObject startGoalPosition;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GlobalManager.Instance.playerSpeed = 0;
            GlobalManager.Instance.turnSpeed = 0f;
            GlobalManager.Instance.rushing = true;

            other.transform.position = startGoalPosition.transform.position;
        }
    }
}
