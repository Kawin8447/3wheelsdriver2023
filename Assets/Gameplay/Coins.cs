using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Coins : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GlobalManager.Instance.coins = GlobalManager.Instance.coins + 2f;
            Destroy(this.gameObject);
            
            SoundManager.Instance.PlaySFX("Coin");
        }
    }
}
