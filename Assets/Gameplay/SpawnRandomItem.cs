using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGD3;
using UnityEngine;

public class SpawnRandomItem : MonoBehaviour
{
    public Collectable collectedRef;
    
    public int maxItems = 8; // Maximum number of items to spawn

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //collectedRef.SpawnRandomItem();
            
            // Spawn items when the game starts
            for (int i = 0; i < maxItems; i++)
            {
                collectedRef.SpawnRandomItem();
            }
        }
    }
}
