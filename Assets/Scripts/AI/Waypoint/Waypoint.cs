using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AI.Waypoint
{
    public class Waypoint : MonoBehaviour
    {
        public Waypoint previousWP;
        public Waypoint nextWP;

        [Range(0f, 5f)] public float width;

        public List<Waypoint> branches;

        [Range(0f, 1f)] public float branchRatio = 0.5f;

        public Vector3 GetPosition()
        {
            Vector3 minBound = transform.position + transform.right * width / 2f;
            Vector3 maxBound = transform.position - transform.right * width / 2f;

            return Vector3.Lerp(minBound, maxBound, Random.Range(0f, 1f));
        }
    }
}