using System;
using System.Collections;
using System.Collections.Generic;
using ProjectGD3;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalManager  : MonoBehaviour
{
    // Create a public static instance of the GlobalManager
    public static GlobalManager Instance { get; private set; }

    // Define your global variable here
    [HideInInspector]
    public int currentZone;

    public float playerSpeed = 15f;

    public float turnSpeed = 7f;

    public float accelerator = 1;

    public int numerRef;
    public string symbolRef;

    public float points = 1;

    public bool rushing = false;

    public float coins = 0;

    public float myCoins = 0;

    public bool panelTrigger = true;

    public float calSumFinalCoins = 1.0f;
    
    private void Awake()
    {
        // Ensure there's only one instance of GlobalManager
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject); // Optional: Keep the object across scenes
        }
        else
        {
            // Destroy duplicates
            Destroy(gameObject);
            Debug.Log("Self Destroying");
        }
    }
}
