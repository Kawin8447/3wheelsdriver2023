using Script;
using TMPro;
using UnityEngine;

namespace Day_night
{
    public class LightingManager : MonoBehaviour
    {
        public static LightingManager Instance { get; private set; }
        
        public Light dLight;
        public LightingPreset m_Preset;

        [SerializeField, Range(0, 8)] public float TimeofDay;
        public  float multipleTime = 0.0165f;

        public GameObject clock_Panel;
        public TextMeshProUGUI clockText;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {
            if (m_Preset == null)
                return;
            
            if (Application.isPlaying)
            {
                TimeofDay += Time.deltaTime * multipleTime;
                TimeofDay %= 8; //Clamp btw 8 am - 4 pm
                UpdateLighting(TimeofDay / 24);
                WorldTimer(TimeofDay);
            }
            else
            {
                UpdateLighting(TimeofDay / 24);
                WorldTimer(TimeofDay);
            }
        }

        private void WorldTimer(float currentTime)
        {
            float minute = Mathf.FloorToInt((currentTime % 60) * 60);
            
            float hours = Mathf.FloorToInt(currentTime % 60);
            
            clockText.text = string.Format("{0:00} : {1:00}",hours + 8f, minute % 60);
        }

        private void UpdateLighting(float timePercent)
        {
            RenderSettings.ambientLight = m_Preset.AmbientColor.Evaluate(timePercent);
            RenderSettings.fogColor = m_Preset.FogColor.Evaluate(timePercent);

            if (dLight != null)
            {
                dLight.color = m_Preset.DirectionalColor.Evaluate(timePercent);
                dLight.transform.localRotation = Quaternion.Euler(new Vector3((timePercent * 360f) + 15f, -170f, 0));
            }
        }
        
        //Try to find a directional light to use if we haven't set one
        private void OnValidDate()
        {
            if (dLight != null)
                return;

            if (RenderSettings.sun != null)
            {
                dLight = RenderSettings.sun;
            }
            else
            {
                Light[] lights = GameObject.FindObjectsOfType<Light>();
                foreach (Light light in lights)
                {
                    if (light.type == LightType.Directional)
                    {
                        dLight = light;
                        return;
                    }
                }
            }
        }
    }
}