using System;
using Unity.VisualScripting;
using UnityEngine;

namespace AI.Waypoint
{
    public class CharacterNavController : MonoBehaviour
    {
        public Animator animator;
        
        public float movementSpeed = 1;
        public float rotationSpeed = 120;
        public float stopDistance = 2.5f;
        
        [SerializeField]
        private Vector3 destination;
        
        private Vector3 velocity;
        private Vector3 lastPosition;

        public bool reachedDestination;

        private void Start()
        {
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            if (transform.position != destination)
            {
                Vector3 destinationDirection = destination - transform.position;
                destinationDirection.y = 0;
                
                animator.SetFloat("Horizontal", 1f);

                float destinationDistance = destinationDirection.magnitude;

                if (destinationDistance >= stopDistance)
                {
                    reachedDestination = false;
                    Quaternion targetRotation = Quaternion.LookRotation(destinationDirection);
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                    transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
                }
                else
                {
                    reachedDestination = true;
                }

                velocity = (transform.position - lastPosition) / Time.deltaTime;
                velocity.y = 0;
                velocity = velocity.normalized;
            }
        }

        public void SetDestination(Vector3 destination)
        {
            this.destination = destination;
            reachedDestination = false;
        }
    }
}