using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Display : MonoBehaviour
{
    //Timer
    [Header("Timer Display")]
    public TextMeshProUGUI timerText;
    private float totalTime = 0f; // Total time in seconds
    private float currentTime;
    
    //Accelerator
    [Header("Accelerator Display")]
    public TextMeshProUGUI acceleratorText;
    
    //Coins
    [Header("Coins Display")]
    public TextMeshProUGUI coin;
    
    //Current System Time
    [Header("Current System Time")]
    public TextMeshProUGUI currentTimeText;

    //SumCoins
    [Header("SumCoins Display")]
    public TextMeshProUGUI calCoins;
    
    void Start()
    {
        //Timer
        currentTime = totalTime;
    }
    
    void Update()
    {
        //Timer
        currentTime += Time.deltaTime;
        UpdateTimerText();
        
        //Accelerator
        acceleratorText.text = ("Accelerator: " + GlobalManager.Instance.accelerator.ToString("#"));
        
        //Coins
        coin.text = ("Coins: " + GlobalManager.Instance.coins);
        
        //Current System Time
        currentTimeText.text = ("CurrentTime: " + currentTime.ToString("#"));
        
        //SumCoins
        calCoins.text = ("Coin x " + GlobalManager.Instance.calSumFinalCoins.ToString("0.00"));
    }
    
    private void UpdateTimerText()
    {
        //Timer
        int minutes = Mathf.FloorToInt(currentTime / 60f);
        int seconds = Mathf.FloorToInt(currentTime % 60f);
        
        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
