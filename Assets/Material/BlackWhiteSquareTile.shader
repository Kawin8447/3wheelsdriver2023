Shader "Custom/BlackWhiteSquareTile"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
 
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float2 texcoord : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }

            half4 frag (v2f i) : SV_Target
            {
                // Create a black and white checker pattern
                half2 tiling = 5; // Adjust this for the size of tiles
                half2 checker = fmod(floor(i.texcoord * tiling), 2);
                half4 color = (checker.x + checker.y) % 2 == 0 ? half4(0, 0, 0, 1) : half4(1, 1, 1, 1);

                return color;
            }
            ENDCG
        }
    }
}
