using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using UnityEngine.Pool;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using Scene = UnityEditor.SearchService.Scene;

namespace Quest
{
    public class NpcQuest : MonoBehaviour
    {
        public bool bounded;
        
        public IObjectPool<NpcQuest> NpcQuestPool { get; set; }
        
        public NavMeshAgent agent;

        public GameObject E_panel;

        public Animator animator;
        
        //public Collider distanceCollider;

        private float _timeToFalseActive = 480.0f;

        public Vector3 v3;
        //public static Transform transformHolder;
        
        private void OnEnable()
        {
            StartCoroutine(TimeFalseActive());
        }

        private void OnDisable()
        {
            ResetQuest();
        }
        
        IEnumerator TimeFalseActive()
        {
            yield return new WaitForSeconds(_timeToFalseActive);
            ReturnToPool();
        }

        private void ReturnToPool()
        {
            NpcQuestPool.Release(this);
        }

        private void ResetQuest()
        {
            Debug.Log("Reset");
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                bounded = !bounded;
                E_panel.SetActive(true);
                animator.SetBool("Bound", true);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                E_panel.SetActive(false);
                animator.SetBool("Bound", false);
            }
        }

        private void Start()
        {
            animator.GetComponent<Animator>();
        }

        private void Update()
        {
            v3 = transform.localPosition;
            
            if (Input.GetKeyDown(KeyCode.E) && bounded)
            {
                bounded = !bounded;
                Debug.Log("Return pool");
                
                ReturnToPool();
            }
        }
    }
}