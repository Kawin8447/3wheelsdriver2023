using System;
using UnityEngine;

namespace Quest
{
    public class IntersectByCar : MonoBehaviour
    {
        private bool check = false;
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                Debug.Log("car bounded");

                check = true;
            }
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.E) && check == true)
            {
                Debug.Log("Accepted");

                check = false;
            }
        }
    }
}