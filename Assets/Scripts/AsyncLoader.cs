using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AsyncLoader : MonoBehaviour
{
    [SerializeField] private GameObject loadingScreen;
    [SerializeField] private GameObject menuScreen;
    
    [Header("Slider")]
    [SerializeField] private Slider m_LoadingSlider;

    public void LoadLevelBTN(string levelToLoad)
    {
        menuScreen.SetActive(false);
        loadingScreen.SetActive(true);
        
        //Run the A sync
        StartCoroutine(LoadLevelSync(levelToLoad));
    }

    IEnumerator LoadLevelSync(string loevelToLoad)
    {
        AsyncOperation loadOP = SceneManager.LoadSceneAsync(loevelToLoad);

        while (!loadOP.isDone)
        {
            float progessValue = Mathf.Clamp01(loadOP.progress / 0.9f);
            m_LoadingSlider.value = progessValue;

            yield return null;
        }
    }
}