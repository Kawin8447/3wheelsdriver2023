using System;
using System.Collections;
using System.Collections.Generic;
using Quest;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.SceneManagement;

public class CallScene : MonoBehaviour
{
    private bool check = false;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("NPC_Quest"))
        {
            Debug.Log("car bounded");

            check = true;
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.E) && check == true)
        {
            Debug.Log("Accepted");

            check = false;

            SceneManager.LoadScene("Scenes/Gameplay");
            //SceneManager.CreateScene("Scenes/Gameplay");
        }
    }
}
