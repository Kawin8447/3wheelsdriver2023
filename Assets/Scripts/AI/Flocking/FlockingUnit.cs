using System;
using System.Collections.Generic;
using System.Numerics;
using Cinemachine;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.ProBuilder;
using UnityEngine.UIElements;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace AI
{
    public class FlockingUnit : MonoBehaviour
    {
        [SerializeField] private float FOVAngle;
        [SerializeField] private float smoothDamp;

        private List<FlockingUnit> cohesionNeighbours = new List<FlockingUnit>();
        private List<FlockingUnit> avoidanceNeighbours = new List<FlockingUnit>();
        private List<FlockingUnit> alignmentNeighbours = new List<FlockingUnit>();
        private Flock assignedFlock;
        private Vector3 currentVelocity;
        private float Speed;
        
        public Transform myTransform { get; set; }

        private void Awake()
        {
            myTransform = transform;
        }

        public void AssignFlock(Flock flock)
        {
            assignedFlock = flock;
        }

        public void InitializeSpeed(float speed)
        {
            this.Speed = speed;
        }

        public void MoveUnit()
        {
            FindNeighbours();
            CalculateSpeed();
            
            var cohesionVector = CalculateCohesionVector() * assignedFlock.cohesionWeight;
            var avoidanceVector = CalculateAvoidanceVector() * assignedFlock.avoidanceWeight;
            var alignmentVector = CalculateAlignmentVector() * assignedFlock.alignmentWeight;

            var moveVector = cohesionVector + avoidanceVector + alignmentVector; // boundsVector;
            moveVector = Vector3.SmoothDamp(myTransform.forward, moveVector, ref currentVelocity, smoothDamp);
            moveVector = moveVector.normalized * Speed;
            myTransform.forward = moveVector;
            myTransform.position += moveVector * Time.deltaTime;
        }
            
        private void CalculateSpeed()
        {
            if (cohesionNeighbours.Count == 0)
                return;
            Speed = 0;
            for (int i = 0; i < cohesionNeighbours.Count; i++)
            {
                Speed += cohesionNeighbours[i].Speed;
            }
            Speed /= cohesionNeighbours.Count;
            Speed = Mathf.Clamp(Speed, assignedFlock.minSpeed, assignedFlock.maxSpeed);
        }

        private void FindNeighbours()
        {
            cohesionNeighbours.Clear();
            avoidanceNeighbours.Clear();
            alignmentNeighbours.Clear();
            var allUnits = assignedFlock.AllUnit;

            for (int i = 0; i < allUnits.Length; i++)
            {
                var currentUnit = allUnits[i];
                if (currentUnit != this)
                {
                    float currentNeighbourDistanceSqr = Vector3.SqrMagnitude(currentUnit.myTransform.position - myTransform.position);
                    if (currentNeighbourDistanceSqr <= assignedFlock.cohesionDistance * assignedFlock.cohesionDistance)
                    {
                        cohesionNeighbours.Add(currentUnit);
                    }
                    if (currentNeighbourDistanceSqr <= assignedFlock.avoidanceDistance * assignedFlock.avoidanceDistance)
                    {
                        avoidanceNeighbours.Add(currentUnit);
                    }
                    if (currentNeighbourDistanceSqr <= assignedFlock.alignmentDistance * assignedFlock.alignmentDistance)
                    {
                        alignmentNeighbours.Add(currentUnit);
                    }
                }
            }
        }

        private Vector3 CalculateCohesionVector()
        {
            var cohesionVector = Vector3.zero;
            if (cohesionNeighbours.Count == 0)
                return cohesionVector;

            int neighboursInFOV = 0;
            for (int i = 0; i < cohesionNeighbours.Count; i++)
            {
                if (IsInFOV(cohesionNeighbours[i].myTransform.position))
                {
                    neighboursInFOV++;
                    cohesionVector += cohesionNeighbours[i].myTransform.position;
                }
            }
            cohesionVector /= neighboursInFOV;
            cohesionVector -= myTransform.position;
            cohesionVector = cohesionVector.normalized;
            return cohesionVector;
        }

        private Vector3 CalculateAvoidanceVector()
        {
            var avoidanceVector = Vector3.zero;
            if (avoidanceNeighbours.Count == 0)
                return Vector3.zero;
            int neighboursInFOV = 0;
            for (int i = 0; i < avoidanceNeighbours.Count; i++)
            {
                if (IsInFOV(avoidanceNeighbours[i].myTransform.position))
                {
                    neighboursInFOV++;
                    avoidanceVector += (myTransform.position - avoidanceNeighbours[i].myTransform.position);
                }
            }
            avoidanceVector /= neighboursInFOV;
            avoidanceVector = avoidanceVector.normalized;
            return avoidanceVector;
        }
        
        private Vector3 CalculateAlignmentVector()
        {
            var alignmentVector = myTransform.forward;
            if (alignmentNeighbours.Count == 0)
                return alignmentVector;
            int neighboursInFOV = 0;
            for (int i = 0; i < alignmentNeighbours.Count; i++)
            {
                if (IsInFOV(alignmentNeighbours[i].myTransform.position))
                {
                    neighboursInFOV++;
                    alignmentVector += alignmentNeighbours[i].myTransform.forward;
                }
            }
            alignmentVector /= neighboursInFOV;
            alignmentVector = alignmentVector.normalized;
            return alignmentVector;
        }

        private bool IsInFOV(Vector3 position)
        {
            return Vector3.Angle(myTransform.forward, position - myTransform.position) <= FOVAngle;
        }
    }
}
