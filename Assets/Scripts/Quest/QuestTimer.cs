using System;
using UnityEngine;
using UnityEngine.UI;

namespace Quest
{
    public class QuestTimer : MonoBehaviour
    {
        public GameObject referencePoint;
        public GameObject destination;

        [SerializeField] private float _distance;
        [SerializeField] private float _timeLeft;
        [SerializeField] private bool timerOn;
        private float multiple_distance = 10f;

        public Text remainingTimeText;

        private void OnEnable()
        {
            timerOn = true;
        }

        private void OnDisable()
        {
            timerOn = false;
            _timeLeft = 0;
            _distance = 0;
        }

        private void Update()
        {
            _distance = (destination.transform.position - referencePoint.transform.position).magnitude * multiple_distance;

            if (Input.GetKey(KeyCode.E))
            {
                _timeLeft = _distance * 0.025f;
            }

            if (timerOn)
            {
                SetTimer(_timeLeft);
                _timeLeft -= Time.deltaTime;
                if (_timeLeft == 0)
                {
                    timerOn = false;
                }
            }
            else
            {
                _timeLeft = 0;
                timerOn = false;
            }
        }

        private void SetTimer(float currentTime)
        {
            float minute = Mathf.FloorToInt(currentTime % 60);
            
            float hours = Mathf.FloorToInt(currentTime / 60);
            
            remainingTimeText.text = string.Format("เวลาที่เหลือ    {0:00} ชั่วโมง  {1:00} นาที",hours, minute);
        }
    }
}