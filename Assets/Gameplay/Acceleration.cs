using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Acceleration : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GlobalManager.Instance.accelerator = GlobalManager.Instance.accelerator + GlobalManager.Instance.points;
            Destroy(this.gameObject);
            
            SoundManager.Instance.PlaySFX("Speed");
        }
    }
}
