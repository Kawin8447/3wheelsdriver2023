using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public GameObject winUI;
    public GameObject shopUI;
    
    public GameObject CloseshopUI;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            ToggleShop();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            addCoins();
        }
    }
    
    public void ToggleShop()
    {
        CloseshopUI.SetActive(!CloseshopUI.activeSelf);
    }

    private void addCoins()
    {
        GlobalManager.Instance.myCoins += 1000;
    }
    
    //on click button do function
    public void Buy()
    {
        if (GlobalManager.Instance.myCoins >= 3000)
        {
            Debug.Log("Buy");
            winUI.SetActive(winUI);
            //shopUI.SetActive(!shopUI.activeSelf);
            Time.timeScale = 0;
        }
        else
        {
            Debug.Log("Not enough coins");
        }
    }
}
