using System;
using TMPro;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

namespace AI
{
    public class Flock : MonoBehaviour
    {
        [Header("SpawnNormal Setup")]
        [SerializeField] private FlockingUnit[] flockUnitPrefabs;
        [SerializeField] private int flockSize;

        [Header("Speed Setup")] 
        [Range(0,10)]
        [SerializeField] private float _minSpeed;
        public float minSpeed{ get { return _minSpeed; }}
        [Range(0,10)]
        [SerializeField] private float _maxSpeed;
        public float maxSpeed{ get { return _maxSpeed; }}

        [Header("Detection Distance")] 
        [Range(0,10)]
        [SerializeField] private float _cohesionDistance;
        public float cohesionDistance => _cohesionDistance;
        [Range(0,10)]
        [SerializeField] private float _avoidanceDistance;
        public float avoidanceDistance => _avoidanceDistance;
        [Range(0,10)]
        [SerializeField] private float _alignmentDistance;
        public float alignmentDistance => _alignmentDistance;
        
        [Header("Behavoiur Weight")] 
        [Range(0,10)]
        [SerializeField] private float _cohesionWeight;
        public float cohesionWeight => _cohesionWeight;
        [Range(0,10)]
        [SerializeField] private float _avoidanceWeight;
        public float avoidanceWeight => _avoidanceWeight;
        [Range(0,10)]
        [SerializeField] private float _alignmentWeight;
        public float alignmentWeight => _alignmentWeight;
        
        public FlockingUnit[] AllUnit { get; set; }

        private void Start()
        {
            GenerateNpc();
        }

        private void Update()
        {
            for (int i = 0; i < AllUnit.Length; i++)
            {
                AllUnit[i].MoveUnit();
            }
        }

        private void GenerateNpc()
        {
            AllUnit = new FlockingUnit[flockSize];

            for (int i = 0; i < flockSize; i++)
            {
                var spawnPosition = new Vector3(Random.Range(-65f, 55f), 1f, Random.Range(-65f, 55f));
                //var rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
                //AllUnit[i] = Instantiate(flockUnitPrefabs, spawnPosition, Quaternion.identity);
                AllUnit[i] = Instantiate(flockUnitPrefabs[Random.Range(0, flockUnitPrefabs.Length)], spawnPosition, Quaternion.identity);
                AllUnit[i].name = "NpcAI";
                AllUnit[i].AssignFlock(this);
                AllUnit[i].InitializeSpeed(Random.Range(_minSpeed, _maxSpeed));
            }
        }
    }
}