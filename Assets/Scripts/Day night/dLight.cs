using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

namespace Day_night
{
    public class dLight : MonoBehaviour
    {
        public static dLight Instance { get; private set; }
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}