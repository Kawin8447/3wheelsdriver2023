using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectGD3
{
    public class Collectable : MonoBehaviour
    {
        public GameObject[] itemPrefabs; // Array of item prefabs to spawn
        public Transform[] spawnPoints; // Array of spawn points (sockets)

        public int maxItems = 8; // Maximum number of items to spawn
        private int spawnedItems = 0;

        private List<int> usedSpawnPointIndices = new List<int>();

        private void Start()
        {
            //SpawnRandomPrefabOnSocket();
        }
        
        public void SpawnRandomItem()
        {
            if (spawnedItems >= maxItems)
            {
                return; // Stop spawning when the maximum limit is reached
            }

            int randomItemIndex = Random.Range(0, itemPrefabs.Length); // Randomly select an item

            // Randomly select a spawn point that hasn't been used yet
            int randomSpawnPointIndex;
            do
            {
                randomSpawnPointIndex = Random.Range(0, spawnPoints.Length);
            } while (usedSpawnPointIndices.Contains(randomSpawnPointIndex));

            usedSpawnPointIndices.Add(randomSpawnPointIndex);

            // Instantiate the randomly selected item at the randomly selected spawn point
            GameObject newItem = Instantiate(itemPrefabs[randomItemIndex], spawnPoints[randomSpawnPointIndex].position, Quaternion.identity);

            spawnedItems++;
        }
    }
}