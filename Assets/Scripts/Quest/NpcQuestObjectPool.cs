using System;
using System.Collections.Generic;
using Car;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Pool;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Quest
{
    public class NpcQuestObjectPool : MonoBehaviour
    {
        public static NpcQuestObjectPool Instance { get; private set; }
        
        public int maxPoolSize = 5;
        public int stackDefaultCapacity = 3;
        
        public GameObject _npcQuestPrefab;

        private IObjectPool<NpcQuest> NpcQuestPool
        {
            get
            {
                if (_npcQuestPool == null)
                    _npcQuestPool = new ObjectPool<NpcQuest>(
                        CreateNpcQuest,
                        TakeFromPool,
                        ReturnToPool,
                        DestroyPool,
                        true,
                        stackDefaultCapacity,
                        maxPoolSize);
                
                return _npcQuestPool;
            }
        }

        private IObjectPool<NpcQuest> _npcQuestPool;
        
        private NpcQuest CreateNpcQuest()
        {
            for (int i = 0; i < stackDefaultCapacity;)
            {
                var go = Instantiate(_npcQuestPrefab, new Vector3(Random.Range(-70f,60f) , 1f, Random.Range(-70f, 60f)), Quaternion.identity);
            
                NpcQuest npcQuest = go.GetComponent<NpcQuest>();
                npcQuest.GetComponent<HeadOutfromRoad>();
                npcQuest.name = "NPC";
                npcQuest.NpcQuestPool = _npcQuestPool;
                
                DontDestroyOnLoad(npcQuest);
                return npcQuest;
            }

            return null;
        }

        private void TakeFromPool(NpcQuest nq)
        {
            nq.gameObject.SetActive(true);
        }

        private void ReturnToPool(NpcQuest nq)
        {
            nq.gameObject.SetActive(false);
        }

        private void DestroyPool(NpcQuest nq)
        {
            Destroy(nq.gameObject);
        }

        private void DoSpawnQuest()
        {
            for (int i = 0; i < stackDefaultCapacity; i++)
            {
                var npcQuest = NpcQuestPool.Get();

                if (npcQuest != null)
                {
                    NpcQuest nq = npcQuest.GetComponent<NpcQuest>();

                    NavMeshTriangulation triangulation = NavMesh.CalculateTriangulation();
                    int VertexIndex = Random.Range(0, triangulation.vertices.Length);
                    NavMeshHit hit;

                    if (NavMesh.SamplePosition(triangulation.vertices[VertexIndex], out hit, 30f, 1))
                    {
                        nq.agent.Warp(hit.position);
                    }
                }
            }
        }

        private void Start()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
                DoSpawnQuest();
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}