using Quest;
using UnityEngine;

namespace Car
{
    public class VariableControll : MonoBehaviour
    {
        [SerializeField] private bool questAccept;

        public GameObject car, npc;

        [SerializeField] private Collider carCollider, npcCollider;
    
        public NpcQuestObjectPool _pool;

        // Start is called before the first frame update
        void Start()
        {
            carCollider = car.GetComponent<Collider>();
            npcCollider = npc.GetComponent<BoxCollider>();

            //navigateArrow.SetActive(false);
        }

        // Update is called once per frame
        private void Update()
        {
            if (carCollider.bounds.Intersects(npcCollider.bounds))
            {
                questAccept = !questAccept;
                Debug.Log("Can accept quest!");
            
                if (Input.GetKey(KeyCode.E))
                {
                    Debug.Log(questAccept);
                }
            }
        }
    }
}
