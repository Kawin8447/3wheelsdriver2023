using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace AI.Waypoint
{
    public class WaypointNavigator : MonoBehaviour
    {
        private CharacterNavController controller;
        public Waypoint currentWP;

        private int direction;

        private void Awake()
        {
            controller = GetComponent<CharacterNavController>();
        }

        private void Start()
        {
            direction = Mathf.RoundToInt(Random.Range(0f, 1f));
            controller.SetDestination(currentWP.GetPosition());
        }

        private void Update()
        {
            if (controller.reachedDestination)
            {
                bool shouldBranch = false;

                if (currentWP.branches != null && currentWP.branches.Count > 0)
                {
                    shouldBranch = Random.Range(0f, 1f) <= currentWP.branchRatio ? true : false;
                }

                if (shouldBranch)
                {
                    currentWP = currentWP.branches[Random.Range(0, currentWP.branches.Count - 1)];
                }
                else
                {
                    if (direction == 0)
                    {
                        if (currentWP.nextWP != null)
                        {
                            currentWP = currentWP.nextWP;
                        }
                        else
                        {
                            currentWP = currentWP.previousWP;
                            direction = 1;
                        }
                    }
                    else if (direction == 1)
                    {
                        if (currentWP.previousWP != null)
                        {
                            currentWP = currentWP.previousWP;
                        }
                        else
                        {
                            currentWP = currentWP.nextWP;
                            direction = 0;
                        }
                    }
                }
                
                controller.SetDestination(currentWP.GetPosition());
            }
        }
    }
}