using System;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEngine;

namespace AI.Waypoint
{
    public class WaypointManager : EditorWindow
    {
        [MenuItem("Tool/Waypoint Editor")]
        public static void Open()
        {
            GetWindow<WaypointManager>();
        }

        public Transform waypointRoot;

        private void OnGUI()
        {
            SerializedObject obj = new SerializedObject(this);

            EditorGUILayout.PropertyField(obj.FindProperty("waypointRoot"));

            if (waypointRoot == null)
            {
                EditorGUILayout.HelpBox("Root transform must be selected. Please assign a toot transform", MessageType.Warning);
            }
            else
            {
                EditorGUILayout.BeginVertical("box");
                DrawButtons();
                EditorGUILayout.EndVertical();
            }

            obj.ApplyModifiedProperties();
        }

        private void DrawButtons()
        {
            if (GUILayout.Button("Create Waypoint"))
            {
                CreateWaypoint();
            }
            if (Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<Waypoint>())
            {
                if (GUILayout.Button("Add Branch Waypoint"))
                {
                    CreateBranchWaypoint();
                }
                if (GUILayout.Button("Create Waypoint Before"))
                {
                    CreateWapointBefore();
                }
                if (GUILayout.Button("Create Waypoint After"))
                {
                    CreateWaypointAfter();
                }
                if (GUILayout.Button("Remove Waypoint"))
                {
                    RemoveWaypoint();
                }
            }
        }

        private void CreateWaypoint()
        {
            GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoint));
            waypointObject.transform.SetParent(waypointRoot, false);

            Waypoint waypoint = waypointObject.GetComponent<Waypoint>();
            if (waypointRoot.childCount > 1)
            {
                waypoint.previousWP = waypointRoot.GetChild(waypointRoot.childCount - 2).GetComponent<Waypoint>();
                waypoint.previousWP.nextWP = waypoint;
                //Place the waypoint at the last position
                waypoint.transform.position = waypoint.previousWP.transform.position;
                waypoint.transform.forward = waypoint.previousWP.transform.forward;
            }

            Selection.activeGameObject = waypoint.gameObject;
        }
        
        private void CreateWapointBefore()
        {
            GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoint));
            waypointObject.transform.SetParent(waypointRoot, false);

            Waypoint newWP = waypointObject.GetComponent<Waypoint>();
            Waypoint selectedWP = Selection.activeGameObject.GetComponent<Waypoint>();

            waypointObject.transform.position = selectedWP.transform.position;
            waypointObject.transform.forward = selectedWP.transform.forward;

            if (selectedWP.previousWP != null)
            {
                newWP.previousWP = selectedWP.previousWP;
                selectedWP.previousWP.nextWP = newWP;
            }

            newWP.nextWP = selectedWP;
            selectedWP.previousWP = newWP;
            newWP.transform.SetSiblingIndex(selectedWP.transform.GetSiblingIndex());
        }

        private void CreateWaypointAfter()
        {
            GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoint));
            waypointObject.transform.SetParent(waypointRoot, false);

            Waypoint newWP = waypointObject.GetComponent<Waypoint>();
            Waypoint selectedWP = Selection.activeGameObject.GetComponent<Waypoint>();

            waypointObject.transform.position = selectedWP.transform.position;
            waypointObject.transform.forward = selectedWP.transform.forward;

            newWP.previousWP = selectedWP;

            if (selectedWP.nextWP != null)
            {
                selectedWP.nextWP.previousWP = newWP;
                newWP.nextWP = selectedWP.nextWP;
            }

            selectedWP.nextWP = newWP;
            newWP.transform.SetSiblingIndex(selectedWP.transform.GetSiblingIndex());
            Selection.activeGameObject = newWP.gameObject;
        }

        private void RemoveWaypoint()
        {
            Waypoint selectedWP = Selection.activeGameObject.GetComponent<Waypoint>();

            if (selectedWP.nextWP != null)
            {
                selectedWP.nextWP.previousWP = selectedWP.previousWP;
            }

            if (selectedWP.previousWP != null)
            {
                selectedWP.previousWP.nextWP = selectedWP.nextWP;
                Selection.activeGameObject = selectedWP.previousWP.gameObject;
            }
            
            DestroyImmediate(selectedWP.gameObject);
        }
        
        private void CreateBranchWaypoint()
        {
            GameObject waypointObject = new GameObject("Waypoint " + waypointRoot.childCount, typeof(Waypoint));
            waypointObject.transform.SetParent(waypointRoot, false);

            Waypoint WP = waypointObject.GetComponent<Waypoint>();

            Waypoint branchFrom = Selection.activeGameObject.GetComponent<Waypoint>();
            branchFrom.branches.Add(WP);

            WP.transform.position = branchFrom.transform.position;
            WP.transform.forward = branchFrom.transform.forward;

            Selection.activeGameObject = WP.gameObject;
        }
    }
}