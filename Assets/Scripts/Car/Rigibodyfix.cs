using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rigibodyfix : MonoBehaviour
{
    [SerializeField] private WheelCollider frontWheelCollider;
    [SerializeField] private WheelCollider rearLeftWheelCollider, rearRightWheelCollider;

    [SerializeField] private float motorForce = 1500f;
    [SerializeField] private float maxSteerAngle = 30f;

    private float horizontalInput, verticalInput;

    private void FixedUpdate()
    {
        GetInput();
        HandleMotor();
        HandleSteering();
        UpdateWheelPoses();
    }

    private void GetInput()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
    }

    private void HandleMotor()
    {
        rearLeftWheelCollider.motorTorque = verticalInput * motorForce;
        rearRightWheelCollider.motorTorque = verticalInput * motorForce;
    }

    private void HandleSteering()
    {
        float steerAngle = maxSteerAngle * horizontalInput;
        frontWheelCollider.steerAngle = steerAngle;
    }

    private void UpdateWheelPoses()
    {
        UpdateWheelPose(frontWheelCollider);
        UpdateWheelPose(rearLeftWheelCollider);
        UpdateWheelPose(rearRightWheelCollider);
    }

    private void UpdateWheelPose(WheelCollider wheelCollider)
    {
        Vector3 pos;
        Quaternion rot;
        wheelCollider.GetWorldPose(out pos, out rot);
        wheelCollider.transform.rotation = rot;
        wheelCollider.transform.position = pos;
    }
}
