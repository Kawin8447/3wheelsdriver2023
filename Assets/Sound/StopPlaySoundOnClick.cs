using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopPlaySoundOnClick : MonoBehaviour
{
    public Slider _musicSlider;
    public Slider _sfxSlider;

    public void ToggleMusic()
    {
        SoundManager.Instance.ToggleMusic();
    }
    
    public void ToggleSFX()
    {
        SoundManager.Instance.ToggleSFX();
    }

    public void MusicVolume()
    {
        SoundManager.Instance.MusicVolume(_musicSlider.value);
    }
    
    public void SFXVolume()
    {
        SoundManager.Instance.SFXVolume(_sfxSlider.value);
    }

    public void StopMusic()
    {
        SoundManager.Instance.musicSource.Stop();
    }
}
